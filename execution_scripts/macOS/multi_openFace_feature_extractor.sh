#!/bin/bash
# script to extract feature with openFace for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process

mkdir tmp_feature_dir

echo >> Processing "'$1/$2'"

mkdir tmp_videos_dir

ffmpeg -i "$1/$2" -filter:v "crop=in_w/2:in_h:in_w/2:in_h" -c:a copy tmp_videos_dir/right_participant.mp4
ffmpeg -i "$1/$2" -filter:v "crop=in_w/2:in_h:0:in_h" -c:a copy tmp_videos_dir/left_participant.mp4


./external_libs/openFace/OpenFace/build/bin/FeatureExtraction -f tmp_videos_dir/right_participant.mp4 -out_dir tmp_feature_dir
./external_libs/openFace/OpenFace/build/bin/FeatureExtraction -f tmp_videos_dir/left_participant.mp4 -out_dir tmp_feature_dir

for file in tmp_feature_dir/*.csv
do
  echo $file
  filename="${file/'tmp_feature_dir/'/''}"
  echo $filename
  filename_no_ext="${filename/.csv/''}"
  echo $filename_no_ext
  echo "$3"/"$filename_no_ext _openFace_feature.csv"
  mv "$file" "$3"/"$filename_no_ext _openFace_feature.csv"
done
rm -r tmp_feature_dir

