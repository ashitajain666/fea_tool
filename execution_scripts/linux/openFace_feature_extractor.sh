#!/bin/bash
# script to extract feature with openFace for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process

mkdir tmp_feature_dir

echo >> Processing "'$1/$2'"

filename=$(basename -- "$2")
extension="${filename##*.}"
filename_no_ext="${filename%.*}"


./external_libs/openFace/OpenFace/build/bin/FeatureExtraction -f "$1/$2" -out_dir tmp_feature_dir

mv "tmp_feature_dir/${filename_no_ext}.csv" "$3"/"${filename_no_ext}_openFace_feature.csv"


rm -r tmp_feature_dir