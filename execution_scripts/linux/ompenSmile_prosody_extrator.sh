#!/bin/bash
# script to extract prsodic features with openSmile for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process

mkdir tmp_feature_dir
./external_libs/openSmile/opensmile/build/progsrc/smilextract/SMILExtract -C config/prosody/prosodyShs.conf -I $1/$2 -O tmp_feature_dir/prosody_extract.csv
./external_libs/openSmile/opensmile/build/bin/FeatureExtraction -f $1/$2 -out_dir tmp_feature_dir

mv tmp_feature_dir/prosody_extract.csv $1/"$"
#cp 'tmp_feature_dir/%s' $1/
for file in tmp_feature_dir/*.csv
do
  mv "$file" $1/"${file%.csv}.openFace_feature"
done
rm -r tmp_feature_dir


SMILExtract -C config/prosody/prosodyShs.conf -I example-audio/opensmile.wav -O test_prosody.csv
