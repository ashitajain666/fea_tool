#!/bin/bash
# script to extract feature with openFace for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process



echo >> Processing "'$1/$2'"

filename=$(basename -- "$2")
extension="${filename##*.}"
filename_no_ext="${filename%.*}"

tmp_dir="tmp_feature_dir${filename_no_ext}"

mkdir ${tmp_dir}
external_libs/openFace/openFace/FeatureExtraction -f "$1/$2" -out_dir ${tmp_dir}

mv "${tmp_dir}/${filename_no_ext}.csv" "$3"/"${filename_no_ext}_openFace_feature.csv"


rm -r ${tmp_dir}

#read  -n 1 -p "Input Selection:" mainmenuinput