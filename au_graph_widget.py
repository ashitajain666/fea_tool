from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel, 
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QGridLayout, QWidget, QStatusBar)
from PyQt5 import QtGui
# graph
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
# data
from scipy import ndimage
import pandas as pd
import numpy as np
import GifPlayer as gp


AU_dict = {
 1  : 	"Inner Brow Raiser", 
 2  : 	"Outer Brow Raiser",
 4  : 	"Brow Lowerer",
 5  : 	"Upper Lid Raiser",
 6  : 	"Cheek Raiser",
 7  : 	"Lid Tightener",
 9  :    "Nose Wrinkler",
 10 :	"Upper Lip Raiser",
 11	:   "Nasolabial Deepener",
 12 :	"Lip Corner Puller", 
 13 :	"Cheek Puffer",
 14 :	"Dimpler",
 15 :	"Lip Corner Depressor",
 16 :   "Lower Lip Depressor",
 17 :	"Chin Raiser",
 18 :	"Lip Puckerer",
 20 :	"Lip stretcher",
 22 :	"Lip Funneler",
 23 :	"Lip Tightener",
 24 :	"Lip Pressor",
 25 :	"Lips part",
 26 :	"Jaw Drop",
 27	:   "Mouth Stretch",
 28 :   "Lip Suck",
 41	:   "Lid droop",
 42 :	"Slit",
 43 :	"Eyes Closed",
 44 :	"Squint",
 45	:   "Blink",
 46 :	"Wink"
}

class au_graph_widget(QtGui.QWidget):
    """
        pyQt widget handler to display a graph with Action Unit value and an image of the corresponding AU
    """

    def __init__(self, auValue: str, auData : np.array, frames : np.array, XgraphSize:int, parent=None):
        super().__init__(parent)
        # super(Quelclient, self).__init__(parent)

        self.auValue = auValue
        self.XgraphSize = XgraphSize
        self.auData = auData
        self.frames = frames

        y_min = np.min(self.auData)
        y_max = np.max(self.auData)
        y_delta = 0.05*(y_max-y_min)
        y_min -=y_delta
        y_max += y_delta
        
        self.label = QLabel(f"%s"%(self.auValue))
        # self.gif_player = gp.GifPlayer(self.auValue)

        self.plotWidget = pg.PlotWidget()
        self.plotWidget.setTitle(f'%s'%(self.auValue))
        self.plotWidget.setFixedHeight(100)
        self.plotWidget.setFixedWidth(200)
        self.plotWidget.setXRange(min=0, max=self.frames[int(self.XgraphSize)])
        self.plotWidget.setYRange(min=y_min, max=y_max)
        self.line = self.plotWidget.addLine(x=0)
        self.plotWidget.plot(frames, auData)
        main_layout = QHBoxLayout()
        gif_layout = QVBoxLayout()
        gif_layout.addWidget(self.label)
        # gif_layout.addWidget(self.gif_player)
        main_layout.addStretch()
        # main_layout.addLayout(gif_layout)
        main_layout.addWidget(self.plotWidget)
        main_layout.addStretch()
        self.setLayout(main_layout)

    def positionChanged(self, position):
        x_min_value = max(0, min(position-int(self.XgraphSize/2), self.frames[-1]-self.XgraphSize))
        x_max_value = min(max(self.XgraphSize, position+int(self.XgraphSize/2)), self.frames[-1])

        print(f'x_max_value : %d = min(max(%d, %d+%d, %d)'%(x_max_value, self.XgraphSize, position, int(self.XgraphSize/2), self.frames[-1] ))
        
        self.plotWidget.setXRange(x_min_value, x_max_value)
        self.line.setValue(position)

    def setXgraphSize(self, XgraphSize:int):
        self.XgraphSize = XgraphSize