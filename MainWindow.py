
from PyQt5.QtWidgets import (QTabWidget, QVBoxLayout, QWidget)
from PyQt5 import QtGui

import VideoPlayer as vp
import FeatureExtractor as fe
import Lib_Installer as li

class MainWindow(QWidget):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        
        self.tabs = QTabWidget()
        self.layout = QVBoxLayout(self)

        # install external libs
        self.libInstaller = li.Libs_Installer(self)   
        
        # tab feature extractor
        self.featureExtractor = fe.FeatureExtractor()
        self.featureExtractor.setWindowTitle("Feature Extractor")

        # tab video viewer and feature analysis
        self.player = vp.VideoPlayer()
        self.player.setWindowTitle("Player")

        # Add tabs
        self.tabs.addTab(self.libInstaller,"Libraries installer")
        self.tabs.addTab(self.featureExtractor,"Feature Extractor")
        self.tabs.addTab(self.player,"Video player with features visualisation")

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    