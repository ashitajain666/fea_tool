# FEA tool : feature extractor and analyzer

This tool is design to help extract feature from a set of videos. It also allows to vizualise the feature and process them 

The tool is still under devlopment 


## How to use

### installation

clone the repository

> git clone https://gitlab.com/Thom/fea_tool.git

then install requirements

> pip install -r requirements.txt

Finally, launch with

> python fea_tool.py

### Usage

Actually, there is 3 panels that can be used

The first one helps install openFace library on ubuntu system. The installation process can be *very* long (more than 1 hour) so please be patient


The second one allows the user to find all video files in a directory. Then, it is possible to watch the video by double clicking the row on the table. It is possible to select multiple rows on the table and then use the *delete* button to remove them. Finally, the bottom button perform feature extraction with openFace and openSmile ComParE_2016 config. The corresponding 'csv' like data are save in the same directory of the file with explicit extension name.

The third and last panel (for now) allow to wathc one video with the corresponding extracted features display on several graphs.


### Overall idea

installation script and execution script of each feature extraction lib are handled by the installer_data.json file. This will allow to add more configuration to this tool and any user can do a Pull Request to add its own