# QMovie *movie=new QMovie("")
# if (!movie->isValid()) 
# {
#     // Something went wrong :(
# }

# // Play GIF
# label=new QLabel(this);
# label->setGeometry(115,60,128,128);
# label->setMovie(movie);
# movie->start();


import sys
from   PyQt5.QtWidgets import *
from   PyQt5.QtGui     import *
from   PyQt5.QtCore    import *


import glob

class GifPlayer(QWidget):
    def __init__(self, auValue: int, widget_size : int = 50, parent=None):
        QWidget.__init__(self, parent)

        au_gif_path = self.getAUGifPath(auValue)
        self.movie = QMovie(au_gif_path, QByteArray(), self)
        self.movie.setScaledSize(QSize().scaled(widget_size, widget_size, Qt.KeepAspectRatio))
        size = self.movie.scaledSize()
        # print("size : "+str(size))
        # size = QtCore.QSize(50, 50)

        # movie = self.loading_lbl.movie()
        # self.movie.setScaledSize(size)
        
        self.setGeometry(50, 50, size.width(), size.height())
        # self.setWindowTitle(f"AU %d : %s"%(auValue, AU_dict[auValue]))
        self.movie_screen = QLabel()
        self.movie_screen.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.movie_screen.setAlignment(Qt.AlignCenter)        
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.movie_screen)
        self.setLayout(main_layout)
        self.movie.setCacheMode(QMovie.CacheAll)
        self.movie_screen.setMovie(self.movie)
        self.movie.start()
        self.movie.loopCount()

    def getAUGifPath(self, auValue:int):
        gif_directory = "./AU_images/"
        print("auValue : "+str(auValue))
        results = glob.glob(f"%sAU%d-*.gif"%(gif_directory, int(auValue)))
        if len(results)==0:
            results = glob.glob(f"%sAU%d.gif"%(gif_directory, int(auValue)))

        print(results)
        return results[0]
    
