import os
import cv2
import time
from PyQt5.QtCore import QDir, QProcess, QSize, Qt, QUrl
from PyQt5.QtGui import QFont, QIcon, QTableWidgetItem, QTextCursor, QLineEdit
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QGridLayout,
                             QHBoxLayout, QLabel, QPushButton, QSizePolicy,
                             QSlider, QStatusBar, QStyle, QTableWidget, QCheckBox,
                             QTextEdit, QVBoxLayout, QWidget, QAbstractScrollArea)
from PyQt5.Qt import QTableWidgetItem, QAbstractItemView
import audiofile
import opensmile
import subprocess
import sys
import shutil
import os
import parselmouth
import audiofile
import pandas as pd
import cv2
import numpy as np
import platform
import datetime
import FeatureLogger as fe
from tqdm import tqdm
# 
accepted_video_extensions = ['mp4', 'flv', 'ts', 'mts', 'avi', 'wmv']


class FeatureExtractor(QWidget):

    def __init__(self, parent=None):
        super(FeatureExtractor, self).__init__(parent)

        # find videos in folder
        find_video_Button = QPushButton("Find videos and audios in folder")   
        find_video_Button.setToolTip("Find videos and audios in folder")
        find_video_Button.setStatusTip("Find videos and audios in folder")
        find_video_Button.clicked.connect(self.find_video_Button_clicked)        

        # install external libs
        extract_features_Button = QPushButton("Extract feature on files")   
        extract_features_Button.setToolTip("Extract feature on files")
        extract_features_Button.setStatusTip("Extract feature on files")
        extract_features_Button.clicked.connect(self.extract_features_Button_clicked)        

        
        self.table = QTableWidget(1, 2, self)
        self.table.setHorizontalHeaderLabels(["Directory", "File"])
        self.table.setSizeAdjustPolicy(
        QAbstractScrollArea.AdjustToContents)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.line_edit = QLineEdit()
        self.line_edit.textChanged.connect(self.filter_table)

        inverse_selection_Button = QPushButton("Inverse seclection")   
        inverse_selection_Button.setToolTip("Inverse seclection")
        inverse_selection_Button.setStatusTip("Inverse seclection")
        inverse_selection_Button.clicked.connect(self.inverse_selection)        


        delete_rows_Button = QPushButton("Remove selected rows from feature extraction process")   
        delete_rows_Button.setToolTip("Remove selected rows from feature extraction process")
        delete_rows_Button.setStatusTip("Remove selected rows from feature extraction process")
        delete_rows_Button.clicked.connect(self.delete_selected_rows)        

        self.extract_dir_label = QLabel("Path to store features")
        self.extract_dir_line_edit = QLineEdit()
        self.extract_dir_button = QPushButton("Select directory to store extracted feature")
        self.extract_dir_button.clicked.connect(self.select_extract_dir)        
        self.extract_dir_layout = QHBoxLayout()
        self.extract_dir_layout.addWidget(self.extract_dir_label)
        self.extract_dir_layout.addWidget(self.extract_dir_line_edit)
        self.extract_dir_layout.addWidget(self.extract_dir_button)

        self.multiFace_horizontal_label = QLabel("Two videos side by side (horizontal) ? ")
        self.multiFace_horizontal_checkbox = QCheckBox('Yes', self)
        self.multiFace_horizontal_layout = QHBoxLayout()
        self.multiFace_horizontal_layout.addWidget(self.multiFace_horizontal_label)
        self.multiFace_horizontal_layout.addWidget(self.multiFace_horizontal_checkbox)

        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.table)
        self.main_layout.addWidget(self.line_edit)
        self.main_layout.addWidget(find_video_Button)
        self.main_layout.addWidget(inverse_selection_Button)
        self.main_layout.addWidget(delete_rows_Button)
        self.main_layout.addLayout(self.multiFace_horizontal_layout)
        self.main_layout.addStretch()
        
        self.main_layout.addLayout(self.extract_dir_layout)
        self.main_layout.addWidget(extract_features_Button)

        self.setLayout(self.main_layout)

        self.table.doubleClicked.connect(self.open_video)

        

    
    def find_video_Button_clicked(self):
        self.root_datapath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        for path, _, files in os.walk(self.root_datapath):
            for name in files:
                if name.split('.')[-1] in accepted_video_extensions:
                    rowPosition = self.table.rowCount()
                    self.table.insertRow(rowPosition) #insert new row

                    self.table.setItem(rowPosition-1, 0, QTableWidgetItem(str(path)))
                    self.table.setItem(rowPosition-1, 1, QTableWidgetItem(str(name)))
        self.table.resizeColumnsToContents()
        self.extractPath = self.root_datapath
        self.extract_dir_line_edit.setText(self.extractPath)


    def select_extract_dir(self):
        self.extractPath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        self.extract_dir_line_edit.setText(self.extractPath)
        

    def open_video(self, mi):
        row = mi.row()
        print(str(mi))
        video_fileName = self.table.item(row, 0).text()+'/'+ self.table.item(row, 1).text()
        print(f'video_fileName : %s'%(video_fileName))
        self.video = QVideoWidget()
        self.video.resize(300, 300)
        self.video.move(0, 0)
        self.player = QMediaPlayer()
        self.player.setVideoOutput(self.video)
        self.player.setMedia(QMediaContent(QUrl.fromLocalFile(video_fileName)))

        self.player.setPosition(0) # to start at the beginning of the video every time
        self.video.show()
        self.player.play()


        mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        mediaPlayer.setMedia(
                    QMediaContent(QUrl.fromLocalFile(video_fileName)))

    def filter_table(self):
        expr = self.line_edit.text()
        expr_in_rows = []
        for row in range(self.table.rowCount()):
            for column in range(self.table.columnCount()):
                _item = self.table.item(row, column) 
                if _item:            
                    item = self.table.item(row, column).text()
                    if expr in item:
                        expr_in_rows.append(row)
        self.selectRows(expr_in_rows)

    def selectRows(self, selection: list):
        for row in range(self.table.rowCount()):
            for column in range(self.table.columnCount()):
                _item = self.table.item(row, column) 
                if _item:            
                    item = self.table.item(row, column)
                    if row in selection:
                        item.setSelected(True)
                    else :
                        item.setSelected(False)
        

    def inverse_selection(self):
        for row in range(self.table.rowCount()):
            for column in range(self.table.columnCount()):
                _item = self.table.item(row, column) 
                if _item:            
                    item = self.table.item(row, column)
                    item.setSelected(not item.isSelected())
                    

    def delete_selected_rows(self):
        indices = self.table.selectionModel().selectedRows() 
    
        print("delete_selected_rows:: index "+str(indices))
        for index in sorted(indices, reverse=True):
            print("delete_selected_rows:: curr_idx "+str(index.row))
            self.table.removeRow(index.row()) 
        
    def extract_features_Button_clicked(self):
        # create log
        current_datetime = datetime.datetime.now()
        log_filename = os.getcwd()+str(f"/logs/log_%s.fea_log"% str(f'{current_datetime:%Y%m%d_%H%M%S%z}'))
        self.log = fe.FeatureLogger(log_filename.replace('\\', '/'))
        self.log['datetime'] = f'{current_datetime:%Y-%m-%d_%H:%M:%S%z}'
        self.log['extractPath'] = self.extractPath
        self.log['root_datapath'] = self.root_datapath
        #iterate over table rows
        ok_files = []
        error_files = []
        for row in range(self.table.rowCount()-1): 
            current_directory = self.table.item(row, 0).text()
            current_file = self.table.item(row, 1).text()
            try :
                if current_directory not in self.log.keys():
                    self.log[current_directory] = {}
                if current_file not in self.log[current_directory].keys():
                    self.log[current_directory][current_file] = {}
                self.process_file(current_file, current_directory)
                ok_files.append(current_directory+'/'+current_file)
            except Exception as e:
                error_files.append(current_directory+'/'+current_file+">> "+str(e))
        #
        print("ok_files : "+str(ok_files))
        print("error_files : "+str(error_files))
        self.log['ok_files'] = ok_files
        self.log['error_files'] = error_files

    def check_feature_file_exists(self, current_file:str, feature_output_dir:str, extension:str):
        file_extension = current_file.split('.')[-1]
        feature_file = feature_output_dir+'/'+current_file.replace('.'+file_extension, extension)
        return os.path.isfile(feature_file) 


    def process_file(self, current_file:str, current_directory:str):
        print(f'Processing %s from %s'%(current_file, current_directory))
        # compute feature ouput directory
        relative_path = current_directory.removeprefix(self.root_datapath)
        feature_output_dir = self.extractPath+'/'+relative_path+'/'
        current_file_extension = current_file.split('.')[-1]
        current_file_no_extension = current_file.replace(current_file_extension, '')
        if not os.path.isdir(feature_output_dir):
            os.makedirs(feature_output_dir, exist_ok=True)
            # os.mkdir(feature_output_dir)
            print("Create feature ouput directory : "+str(feature_output_dir))
        # prepare log 
        if current_directory not in self.log.keys():
            self.log[current_directory] = {}
        if current_file_no_extension not in self.log[current_directory].keys():
            self.log[current_directory][current_file_no_extension] = {}
        print("self.log : "+str(self.log))
        if self.multiFace_horizontal_checkbox.isChecked():
            print(" >>> process_multi_openFace <<<")
            if self.check_feature_file_exists(current_file, feature_output_dir, '_left_openFace_feature.csv')==False and self.check_feature_file_exists(current_file, feature_output_dir, '_right_openFace_feature.csv')==False:
                self.process_multi_openFace(current_file, current_directory, feature_output_dir)
        else:
            if self.check_feature_file_exists(current_file, feature_output_dir, '_openFace_feature.csv')==False :
                self.process_openFace(current_file, current_directory, feature_output_dir)
        print(">> openface done")
        if self.check_feature_file_exists(current_file, feature_output_dir, '_openSmile.csv')==False :
            self.process_openSmile(current_file, current_directory, feature_output_dir)
        print(">> openSmile done")
        if self.check_feature_file_exists(current_file, feature_output_dir, '_prosogram.csv')==False : 
            self.process_prosogram(current_file, current_directory, feature_output_dir)
        print(">> prosogram done")
        

    def process_openFace(self, current_file:str, current_directory: str, feature_output_dir:str, original_video_directory:str = None):
        # openFace execution
        openface_log = "openFace_log.txt"
        plt = "Windows" if platform.system() else "linux"

        cmd = str(f"./execution_scripts/%s/openFace_feature_extractor.sh '%s' '%s' '%s' >> '%s'"%(plt, current_directory, current_file, feature_output_dir, openface_log))
        cmd = 'start /W '+cmd if plt=="Windows" else cmd
        print(">> process_openFace cmd : "+str(cmd))
        exit_status = os.system(cmd)
        split_file = 'full'
        log_filename = current_file
        original_video_directory = current_directory if original_video_directory==None else original_video_directory
        if self.multiFace_horizontal_checkbox.isChecked():
            if '_right' in current_file : 
                log_filename = current_file.replace('_right','')
                split_file = 'right'
            elif '_left'  in current_file : 
                log_filename = current_file.replace('_left','')
                split_file = 'right'
        #
        extension = log_filename.split('.')[-1]
        log_filename = log_filename.replace(extension, '')
        print("log_filename : %s"%(log_filename))
        if os.path.isfile(openface_log):
            print(">>>> open OF log")
            f1 = open(openface_log, "r")
            print(">>>> OF log opened")
            openFace_feature_file = f1.readlines()[-1].replace('\n', '')
            f1.close()
            print("openFace_feature_file : "+str(openFace_feature_file))
            print(">> self.log")
            for keys,values in self.log.items():
                print(">>" + str(keys))
                print(">>" + str(values))
                print('*'*5)
            print("*"*42)
            print(">> self.log[%s]"%(original_video_directory))
            
            test = "%s in log[%s]"%(log_filename , original_video_directory) if log_filename in self.log[original_video_directory].keys() else "%s not in log[%s]"%(log_filename , original_video_directory)
            print(test)

            if 'openFace' in self.log[original_video_directory][log_filename].keys():
                feature_file_name_dict = self.log[original_video_directory][log_filename]['openFace']['feature_file_name']
                feature_file_name_dict[split_file] = openFace_feature_file
                feature_file_name_dict['exit_status_'+str(split_file)] = exit_status
                self.log[original_video_directory][log_filename]['openFace']['feature_file_name'] = feature_file_name_dict
            else:
                print(">> "+str('exit_status_'+str(split_file)))
                self.log[original_video_directory][log_filename]['openFace'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' :{split_file : openFace_feature_file, 'exit_status_'+str(split_file) : exit_status }}
            
            print(">>>> log updated")
            if exit_status != 0:
                print(str(exit_status)) 
                raise ValueError("openFace raised error : '%s'."%(exit_status))
        else:
            print("openFace log : '%s' does not exist."%(openface_log))
            raise ValueError("openFace log : '%s' does not exist."%(openface_log))
        print("[%s] has been executed correctly"%(cmd))
        return 0



    def process_multi_openFace(self, current_file:str, current_directory: str, feature_output_dir:str):
        # openFace execution
        openface_log = "openFace_log.txt"
        multi_openface_tmp_dir = "multi_openface_tmp_dir/"
        if not os.path.isdir(multi_openface_tmp_dir):
            os.makedirs(multi_openface_tmp_dir, exist_ok=True)

        # Open the video
        cap = cv2.VideoCapture(current_directory+'/'+current_file)

        # Some characteristics from the original video
        w_frame, h_frame = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps, frames = cap.get(cv2.CAP_PROP_FPS), cap.get(cv2.CAP_PROP_FRAME_COUNT)
        no_extension_filename = current_file.split('.')[-2]

        # Here you can define your croping values
        x_left,y_left, x_right,y_right, h,w = 0,0, int(w_frame/2),0, h_frame,int(w_frame/2)

        # output
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out_left = cv2.VideoWriter(multi_openface_tmp_dir+no_extension_filename+'_left.mp4', fourcc, fps, (w, h))
        out_right = cv2.VideoWriter(multi_openface_tmp_dir+no_extension_filename+'_right.mp4', fourcc, fps, (w, h))

        # Initialize frame counter
        cnt = 0
        pbar = tqdm(total=100)
        # frames = 25 ### DEBUG !!!!!!
        # Now we start
        while(cap.isOpened()):
            ret, frame = cap.read()
            cnt += 1 # Counting frames

            # Avoid problems when video finish
            if ret==True:
                # Croping the frame
                crop_frame_left = frame[y_left:y_left+h, x_left:x_left+w]
                crop_frame_right = frame[y_right:y_right+h, x_right:x_right+w]
                # Here you save all the video
                out_left.write(crop_frame_left)
                out_right.write(crop_frame_right)
                        # Percentage
                xx = cnt *100/frames
                # print(int(xx),'%')
                pbar.update(100/frames)

            if cnt>frames:
                break


        cap.release()
        out_left.release()
        out_right.release()

        print("process_multi_openFace::spllit done")

        self.process_openFace(no_extension_filename+'_left.mp4', multi_openface_tmp_dir, feature_output_dir, original_video_directory=current_directory)
        print("%s left part done "%(no_extension_filename))
        self.process_openFace(no_extension_filename+'_right.mp4', multi_openface_tmp_dir, feature_output_dir, original_video_directory=current_directory)

        print("%s right part done "%(no_extension_filename))


        shutil.rmtree(multi_openface_tmp_dir)

        # # filter AUs to keep Smile, Eyebrow, Head Nod and gaze
        # cols_to_keep = ['frame', 'face_id', 'timestamp', 'confidence', 'success', 'gaze_0_x', 'gaze_0_y', 'gaze_0_z', 'gaze_1_x', 'gaze_1_y', 'gaze_1_z', 'gaze_angle_x', 'gaze_angle_y',  'pose_Tx', 'pose_Ty', 'pose_Tz', 'pose_Rx', 'pose_Ry', 'pose_Rz', 'AU01_r', 'AU02_r', 'AU04_r',  'AU06_r', 'AU12_r', 'AU01_c', 'AU02_c', 'AU04_c', 'AU06_c', 'AU12_c']
        # left_filename = feature_output_dir+no_extension_filename+'_left_openFace_feature.csv'
        # try :
        #     left_df = pd.read_csv(left_filename)
        #     left_df = left_df[cols_to_keep]
        # except:
        #     arr = np.zeros((len(cols_to_keep), frames))
        #     left_df = pd.DataFrame(arr)
        #     left_df.columns = cols_to_keep
        # #
        # left_df.to_csv(left_filename)
        # right_filename = feature_output_dir+no_extension_filename+'_right_openFace_feature.csv'
        # try:
        #     right_df = pd.read_csv(right_filename)
        #     right_df = right_df[cols_to_keep]
        # except:
        #     arr = np.zeros((len(cols_to_keep), frames))
        #     right_df = pd.DataFrame(arr)
        #     right_df.columns = cols_to_keep
        # #
        # right_df.to_csv(right_filename)

        return 0
        
    def process_openSmile(self, current_file:str, current_directory:str, feature_output_dir:str):
        #openSmile execution
        file_extension = current_file.split('.')[-1]
        current_file_no_ext = current_file.replace(file_extension, '')
        try :
            filename = str(f'%s/%s'%(current_directory, current_file))
            print('>> process_openSmile %s'%(filename))
            signal, sampling_rate = audiofile.read(filename, always_2d=True)
            num_channels = signal.data.shape[0]
            smile = opensmile.Smile(
            feature_set=opensmile.FeatureSet.eGeMAPSv02,
            num_channels=num_channels,
            feature_level=opensmile.FeatureLevel.LowLevelDescriptors,
            )
            cam = cv2.VideoCapture(filename)
            video_fps = cam.get(cv2.CAP_PROP_FPS)
            prosody_data = smile.process_signal(
                signal,
                sampling_rate
            )
            prosody_data.reset_index(inplace=True)
            prosody_data['timestamp'] = ((prosody_data['start']+prosody_data['end'])/2.0) /np.timedelta64(1, 's')
            prosody_data['frame'] = (prosody_data['timestamp']*video_fps).astype(int)
            prosody_data = prosody_data.groupby('frame').mean().reset_index() # mean when multiple values for one video frame (different sampling audio video)
            
            feature_file_name = str(feature_output_dir+'/'+current_file).replace('.'+file_extension, '_openSmile.csv')
            prosody_data.to_csv(feature_file_name)
            
            self.log[current_directory][current_file_no_ext]['openSmile'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : feature_file_name, 'exit_status' : 'ok' }
        
        except OSError as err:
            print("OS error: {0}".format(err))
            self.log[current_directory][current_file_no_ext]['openSmile'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : '', 'exit_status' : err }
            return -1
        except subprocess.CalledProcessError as err:
            print(f"no sound on video %s"%(filename))
            self.log[current_directory][current_file_no_ext]['openSmile'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : '', 'exit_status' : err }
            return -1
        except ValueError:
            print("Could not convert data to an integer.")
            self.log[current_directory][current_file_no_ext]['openSmile'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : '', 'exit_status' : "ValueError" }
            return -1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            self.log[current_directory][current_file_no_ext]['openSmile'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : '', 'exit_status' : sys.exc_info()[0] }
            return -1
        return 0


    def compute_f0(self, timestep, prosogram_df):
        prosogram_value = prosogram_df.loc[(prosogram_df['nucl_t1'] <= timestep) ] 
        if len(prosogram_value) == 0 :
            # not speaking
            return 0
        else:
            prosogram_value = prosogram_value.sort_values(by = 'nucl_t1', ascending = False)
            value = prosogram_value.iloc[0]
            if value['nucl_t2'] >= timestep :
                f0_value = value['f0_start'] + (timestep-value['nucl_t1'])*(value['f0_end']-value['f0_start'])/(value['nucl_t2']-value['nucl_t1'])
                return f0_value
            elif value['before_pause'] == 0:
                second_prosogram_value = prosogram_df.loc[(prosogram_df['nucl_t1'] > timestep) ].copy(deep=True)
                second_prosogram_value =second_prosogram_value.sort_values(by = 'nucl_t1', ascending = True)
                second_value = second_prosogram_value.iloc[0]
                # compute value
                f0_value = second_value['f0_end'] + (timestep-value['nucl_t2'])*(second_value['f0_start']-value['f0_end'])/(second_value['nucl_t1']-value['nucl_t2'])
                return f0_value
            else : 
                # not speaking
                return 0
        return 0



    def process_prosogram(self, current_file:str, current_directory:str, feature_output_dir:str):
        #prosogram execution
        working_dir = 'tmp_dir'
        if not os.path.isdir(working_dir):
            os.mkdir(working_dir)
        filename = str(f'%s/%s'%(current_directory, current_file))
        print('>> process_prosogram %s'%(filename))
        file_extension = current_file.split('.')[-1]
        current_file_no_ext = current_file.replace(file_extension, '')
        #
        # extension = current_file.split('.')[-1]
        root_filename = ''.join(current_file.rsplit(file_extension, 1))
        #
        signal, sampling_rate = audiofile.read(filename, always_2d=True)
        num_channels = signal.data.shape[0]
        #
        full_prosogram_df = pd.DataFrame()
        for channel in range(num_channels):
            export_audio_filename = os.getcwd()+'/'+working_dir+'/C_'+str(channel)+root_filename+'wav'
            audiofile.write(export_audio_filename, signal[channel], sampling_rate)
            prosogram_praat_script = os.getcwd()+'/external_libs/prosogram/prosomain_fea_tool.praat'
            parselmout_error = False
            try:
                parselmouth.praat.run_file(prosogram_praat_script, export_audio_filename)
            except:
                parselmout_error = True
            if not parselmout_error:
                prosogram_csv_data = export_audio_filename.replace('.wav','_data.txt')
                if os.path.isfile(prosogram_csv_data):
                    df = pd.read_csv(prosogram_csv_data, sep='\t')
                    # fill prosdic data
                    prosogram_df = df[['nucl_t1', 'nucl_t2', 'after_pause', 'before_pause', 'f0_start', 'f0_end']]
                else:
                    prosogram_df = pd.DataFrame(columns = ['nucl_t1', 'nucl_t2', 'after_pause', 'before_pause', 'f0_start', 'f0_end'])
                cam = cv2.VideoCapture(filename)
                video_fps = cam.get(cv2.CAP_PROP_FPS)
                nb_frames = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
                # fill prosodic value with interpolation on F0 values on timestep and 0 on pauses
                prosodic_df = pd.DataFrame()
                prosodic_df['frame'] = np.arange(nb_frames)
                prosodic_df['timestep'] = prosodic_df['frame']/video_fps
                #
                prosodic_df['f0'] = prosodic_df['timestep'].apply(self.compute_f0, prosogram_df=prosogram_df)
                if len(full_prosogram_df)==0 :
                    full_prosogram_df = prosodic_df
                else:
                    prosodic_df.drop(columns = 'frame', inplace=True)
                    full_prosogram_df = pd.merge_asof(full_prosogram_df, prosodic_df, on='timestep', direction='forward', tolerance=1/video_fps, suffixes=("_"+str(channel-1), "_"+str(channel)))
        #save
        export_filename = current_file.replace('.'+file_extension, '_prosogram.csv')
        try:
            print("Export prosogram to "+feature_output_dir+'/'+export_filename)
            print("full_prosogram_df shape  : "+str(full_prosogram_df.shape))
            full_prosogram_df.to_csv(feature_output_dir+'/'+export_filename)
            self.log[current_directory][current_file_no_ext]['prosogram'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : feature_output_dir+'/'+export_filename, 'exit_status' : "ok" }
            
        except:
            print("Error full_prosogram_df.to_csv")
            current_file_no_ext = current_file.replace(file_extension, '')
            self.log[current_directory][current_file_no_ext]['prosogram'] = {'feature_output_dir' : feature_output_dir, 'feature_file_name' : "", 'exit_status' : "Error full_prosogram_df.to_csv" }
            
        try:
            shutil.rmtree(working_dir)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
            return -1
        return 0


if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = FeatureExtractor()()
    clock.show()
    sys.exit(app.exec_())
